package com.johnriggsdev.malauzai

import com.johnriggsdev.malauzai.models.Apod

@Suppress("unused")
class TestData {
    companion object {
        const val GOOD_APOD_JSON = "[{\"title\":\"Weird Star\",\"copyright\":\"Blorty Blortson\",\"date\":\"1997-05-22\",\"explanation\":\"Scientists are baffled. BAFFLED!\",\"url\":\"https://images.newscientist.com/wp-content/uploads/2019/07/02111520/gettyimages-157639696.jpg\",\"hdurl\":\"https://images.newscientist.com/wp-content/uploads/2019/07/02111520/gettyimages-157639696.jpg\",\"media_type\":\"image\"}," +
                "{\"title\":\"Interesting Star\",\"copyright\":\"Dick Fightmaster\",\"date\":\"1980-11-23\",\"explanation\":\"Scientists are interested!\",\"url\":\"https://www.universetoday.com/wp-content/uploads/2008/10/m45.jpg\",\"hdurl\":\"https://www.universetoday.com/wp-content/uploads/2008/10/m45.jpg\",\"media_type\":\"image\"}]"

        const val GOOD_APOD_JSON_MIXED_MEDIA = "[{\"title\":\"Weird Star\",\"copyright\":\"Blorty Blortson\",\"date\":\"1997-05-22\",\"explanation\":\"Scientists are baffled. BAFFLED!\",\"url\":\"https://images.newscientist.com/wp-content/uploads/2019/07/02111520/gettyimages-157639696.jpg\",\"hdurl\":\"https://images.newscientist.com/wp-content/uploads/2019/07/02111520/gettyimages-157639696.jpg\",\"media_type\":\"image\"}," +
                "{\"title\":\"Interesting Star\",\"copyright\":\"Dick Fightmaster\",\"date\":\"1980-11-23\",\"explanation\":\"Scientists are interested!\",\"url\":\"https://www.universetoday.com/wp-content/uploads/2008/10/m45.jpg\",\"hdurl\":\"https://www.universetoday.com/wp-content/uploads/2008/10/m45.jpg\",\"media_type\":\"video\"}]"

        const val BAD_APOD_JSON = "[{\"title\":\"Weird Star\",\"copyright\":\"Blorty Blortson\",\"date\":\"1997-05-22\",\"explanation\":\"Scientists are baffled. BAFFLED!\",\"url\":\"https://images.newscientist.com/wp-content/uploads/2019/07/02111520/gettyimages-157639696.jpg\",\"hdurl\":\"https://images.newscientist.com/wp-content/uploads/2019/07/02111520/gettyimages-157639696.jpg\",\"media_type\":\"image\"}" +
                "{\"title\":\"Interesting Star\",\"copyright\":\"Dick Fightmaster\",\"date\":\"1980-11-23\",\"explanation\":\"Scientists are interested!\",\"url\":\"https://www.universetoday.com/wp-content/uploads/2008/10/m45.jpg\",\"hdurl\":\"https://www.universetoday.com/wp-content/uploads/2008/10/m45.jpg\",\"media_type\":\"image\"}]"

        const val BAD_APOD_JSON_MIXED_MEDIA = "[{\"title\":\"Weird Star\",\"copyright\":\"Blorty Blortson\",\"date\":\"1997-05-22\",\"explanation\":\"Scientists are baffled. BAFFLED!\",\"url\":\"https://images.newscientist.com/wp-content/uploads/2019/07/02111520/gettyimages-157639696.jpg\",\"hdurl\":\"https://images.newscientist.com/wp-content/uploads/2019/07/02111520/gettyimages-157639696.jpg\",\"media_type\":\"image\"}" +
                "{\"title\":\"Interesting Star\",\"copyright\":\"Dick Fightmaster\",\"date\":\"1980-11-23\",\"explanation\":\"Scientists are interested!\",\"url\":\"https://www.universetoday.com/wp-content/uploads/2008/10/m45.jpg\",\"hdurl\":\"https://www.universetoday.com/wp-content/uploads/2008/10/m45.jpg\",\"media_type\":\"video\"}]"

        const val TIMEOUT_SLEEP_MILLIS = 15000

        const val SOME_ERROR_STRING = "Some error"

        val APOD_1 = Apod("https://images.newscientist.com/wp-content/uploads/2019/07/02111520/gettyimages-157639696.jpg", "Weird Star", "Blorty Blortdon", "Scientists are baffled. BAFFLED!", "1997-05-22", "https://images.newscientist.com/wp-content/uploads/2019/07/02111520/gettyimages-157639696.jpg", "image")
        val APOD_2 = Apod("https://www.universetoday.com/wp-content/uploads/2008/10/m45.jpg", "Interesting Star", "Dick Fightmaster", "Scientists are interested!", "1980-11-23", "https://www.universetoday.com/wp-content/uploads/2008/10/m45.jpg", "image")

        val APOD_ARRAY = arrayOf(APOD_1, APOD_2)
        val EMPTY_APOD_ARRAY : Array<Apod> = arrayOf()
    }
}