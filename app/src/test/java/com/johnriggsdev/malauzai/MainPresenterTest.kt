package com.johnriggsdev.malauzai

import android.util.Log
import com.johnriggsdev.malauzai.TestData.Companion.APOD_ARRAY
import com.johnriggsdev.malauzai.TestData.Companion.EMPTY_APOD_ARRAY
import com.johnriggsdev.malauzai.TestData.Companion.SOME_ERROR_STRING
import com.johnriggsdev.malauzai.ui.main.MainActivity
import com.johnriggsdev.malauzai.ui.main.MainPresenter
import com.johnriggsdev.malauzai.ui.main.MainRepository
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.powermock.api.mockito.PowerMockito
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner

@RunWith(PowerMockRunner::class)
@PrepareForTest(Log::class)
class MainPresenterTest{

    private lateinit var mockRepo : MainRepository
    private lateinit var mockView : MainActivity
    private lateinit var presenter : MainPresenter

    @Before
    fun setup(){
        PowerMockito.mockStatic(Log::class.java)

        mockRepo = mock()
        mockView = mock()
        presenter = MainPresenter(mockRepo)

        presenter.attach(mockView)
    }

    @Test
    fun `attaching view sets callback in repo`(){
        verify(mockRepo).setNetworkCallback(presenter)
    }

    @Test
    fun `onDestroy nullifies view reference`(){
        assert(presenter.getViewForTesting() != null)
        presenter.onDestroy()
        assert(presenter.getViewForTesting() == null)
    }

    @Test
    fun `view instructed to show toast on api error`(){
        presenter.onApiError(SOME_ERROR_STRING)
        verify(mockView).showErrorToast(SOME_ERROR_STRING)
    }

    @Test
    fun `presenter returns appropriate array upon rotation`(){
        presenter.onApodsDbSuccess(APOD_ARRAY)
        val array = presenter.getApodsForScreenRotation()

        assert(!array.contentEquals(EMPTY_APOD_ARRAY))
        assert(array.contentEquals(APOD_ARRAY))
    }

    @Test
    fun `verify methods called on activity recreated`(){
        presenter.onActivityRecreated(APOD_ARRAY)

        verify(mockView).setupRecyclerView(APOD_ARRAY)
        verify(mockView).showProgressBar(false)
    }

    @Test
    fun `verify methods called on new activity created`(){
        presenter.onNewActivityCreated()

        verify(mockView).showProgressBar(true)
    }

    @Test
    fun `db success sets array and calls view methods`(){
        presenter.onApodsDbSuccess(APOD_ARRAY)

        assert(presenter.getArrayForTesting().contentEquals(APOD_ARRAY))

        verify(mockView).setupRecyclerView(APOD_ARRAY)
        verify(mockView).showProgressBar(false)
    }

    @Test
    fun `db failure requests repo to call api`(){
        presenter.onApodsDbFailure()

        verify(mockRepo).getApodsFromApi(false)
    }

    @Test
    fun `api success sets array and calls view methods`(){
        presenter.onApodsApiSuccess(APOD_ARRAY, false)

        assert(presenter.getArrayForTesting().contentEquals(APOD_ARRAY))

        verify(mockView).setupRecyclerView(APOD_ARRAY)
        verify(mockView).showProgressBar(false)

        presenter.onApodsApiSuccess(APOD_ARRAY, true)

        verify(mockView).updateRecyclerView(APOD_ARRAY)
        verify(mockView, times(2)).showProgressBar(false)
    }

    @Test
    fun `api failure requests repo to call api`(){
        Mockito.`when`(mockRepo.getString(R.string.no_api_photos)).thenReturn(SOME_ERROR_STRING)

        presenter.onApodsApiFailure()

        verify(mockView).showErrorToast(SOME_ERROR_STRING)
        verify(mockView).showProgressBar(false)
    }

    @Test
    fun `verify methods called on swipe refresh`(){
        Mockito.`when`(mockRepo.getApodsFromApi(true)).then{
            presenter.onApodsApiSuccess(APOD_ARRAY, true)
        }

        presenter.onSwipeRefreshTriggered()

        verify(mockView).hideSwipeRefreshSpinner()
        verify(mockView).showProgressBar(true)

        verify(mockRepo).getApodsFromApi(true)

        verify(mockView).updateRecyclerView(APOD_ARRAY)
        verify(mockView).showProgressBar(false)
    }
}