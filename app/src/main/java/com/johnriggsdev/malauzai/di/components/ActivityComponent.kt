package com.johnriggsdev.malauzai.di.components

import com.johnriggsdev.malauzai.di.modules.ActivityModule
import com.johnriggsdev.malauzai.ui.main.MainActivity
import com.johnriggsdev.malauzai.ui.main.MainPresenter
import dagger.Component

@Component(modules = [ActivityModule::class])
interface ActivityComponent {

    fun inject(mainActivity: MainActivity)

    fun inject(mainPresenter: MainPresenter)
}