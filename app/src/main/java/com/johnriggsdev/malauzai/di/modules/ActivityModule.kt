package com.johnriggsdev.malauzai.di.modules

import android.support.v7.app.AppCompatActivity
import com.johnriggsdev.malauzai.ui.main.MainContract
import com.johnriggsdev.malauzai.ui.main.MainPresenter
import com.johnriggsdev.malauzai.ui.main.MainRepository
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private var activity: AppCompatActivity) {
    @Provides
    fun provideActivity(): AppCompatActivity {
        return activity
    }

    @Provides
    fun providePresenter(): MainContract.Presenter {
        return MainPresenter(provideRepository())
    }

    @Provides
    fun provideRepository(): MainContract.Repository {
        return MainRepository()
    }
}