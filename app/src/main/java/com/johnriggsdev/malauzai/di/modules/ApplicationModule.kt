package com.johnriggsdev.malauzai.di.modules

import android.app.Application
import com.johnriggsdev.malauzai.app.MalauzaiApp
import com.johnriggsdev.malauzai.di.scopes.PerApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val malauzaiApp: MalauzaiApp) {

    @Provides
    @Singleton
    @PerApplication
    fun provideAllication() : Application{
        return malauzaiApp
    }
}