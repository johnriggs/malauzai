package com.johnriggsdev.malauzai.di.components

import com.johnriggsdev.malauzai.app.MalauzaiApp
import com.johnriggsdev.malauzai.di.modules.ApplicationModule
import dagger.Component

@Component(modules = [ApplicationModule::class])
interface ApplicationComponent {
    fun inject(application: MalauzaiApp)
}