package com.johnriggsdev.malauzai.app

import android.app.Application
import com.johnriggsdev.malauzai.BuildConfig
import com.johnriggsdev.malauzai.di.components.ApplicationComponent
import com.johnriggsdev.malauzai.di.components.DaggerApplicationComponent
import com.johnriggsdev.malauzai.di.modules.ApplicationModule

class MalauzaiApp : Application() {

    private lateinit var component: ApplicationComponent

    override fun onCreate() {
        super.onCreate()

        instance = this
        setup()

        if (BuildConfig.DEBUG) {
            // Maybe TimberPlant etc.
        }
    }

    // Method call currently appears deprecated only because the ApplicationComponent is not called in the project
    // at this time. That can change in the future.
    @Suppress("deprecation")
    private fun setup() {
        component = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this)).build()
        component.inject(this)
    }

    @Suppress("unused")
    fun getApplicationComponent(): ApplicationComponent {
        return component
    }

    companion object {
        lateinit var instance: MalauzaiApp private set
    }
}