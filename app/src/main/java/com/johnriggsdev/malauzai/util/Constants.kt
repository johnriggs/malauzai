package com.johnriggsdev.malauzai.util

class Constants {
    companion object {
        const val APP_TAG = "Malauzai App"
        const val NASA_API_KEY = "api_key=YPj8DkW7TTPE9OkWSsDiOhrQfEs7CnlZSHbhAELQ"
        const val NASA_API_COUNT = "count=25"
        const val NASA_API_APOD = "apod?"
        const val NASA_BASE_URL = "https://api.nasa.gov/planetary/"
        const val EMPTY_STRING =""
    }
}