package com.johnriggsdev.malauzai.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Apod (
    val url: String,
    val title: String,
    @SerializedName("copyright") val author: String?,
    @SerializedName("explanation") val description: String,
    val date: String,
    @SerializedName("hdurl") val urlhd : String,
    @SerializedName("media_type") val mediaType: String) : Parcelable