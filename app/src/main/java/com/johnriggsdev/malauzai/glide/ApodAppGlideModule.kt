package com.johnriggsdev.malauzai.glide

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@Suppress("unused")
@GlideModule
class MyAppGlideModule : AppGlideModule()