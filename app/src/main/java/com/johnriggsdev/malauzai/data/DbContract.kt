package com.johnriggsdev.malauzai.data

import android.provider.BaseColumns

class DbContract {
    companion object ApodTable : BaseColumns{
        const val TABLE_NAME = "name"
        const val COLUMN_TITLE = "title"
        const val COLUMN_AUTHOR = "author"
        const val COLUMN_DATE = "date"
        const val COLUMN_DESC = "description"
        const val COLUMN_URL = "url" // Primary Key
        const val COLUMN_URL_HD = "url_hd"
        const val COLUMN_MEDIA_TYPE = "media_type"
    }
}