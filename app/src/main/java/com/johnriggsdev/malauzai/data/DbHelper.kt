package com.johnriggsdev.malauzai.data

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.johnriggsdev.malauzai.util.Constants.Companion.APP_TAG
import com.johnriggsdev.malauzai.data.DbContract.ApodTable.COLUMN_AUTHOR
import com.johnriggsdev.malauzai.data.DbContract.ApodTable.COLUMN_DATE
import com.johnriggsdev.malauzai.data.DbContract.ApodTable.COLUMN_DESC
import com.johnriggsdev.malauzai.data.DbContract.ApodTable.COLUMN_MEDIA_TYPE
import com.johnriggsdev.malauzai.data.DbContract.ApodTable.COLUMN_TITLE
import com.johnriggsdev.malauzai.data.DbContract.ApodTable.COLUMN_URL
import com.johnriggsdev.malauzai.data.DbContract.ApodTable.COLUMN_URL_HD
import com.johnriggsdev.malauzai.data.DbContract.ApodTable.TABLE_NAME
import org.json.JSONArray
import org.json.JSONException
import kotlin.text.StringBuilder

class DbHelper(context: Context) : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {

    companion object {
        private const val DB_NAME = "apod.db"
        private var DB_VERSION = 1
        private const val IMAGE_TYPE = "image"

        private const val QUERY_APODS = "SELECT * FROM $TABLE_NAME"

        const val SQL_CREATE_APOD_TABLE = "CREATE TABLE ${DbContract.TABLE_NAME} (" +
                "${DbContract.COLUMN_URL} TEXT PRIMARY KEY, " +
                "${DbContract.COLUMN_TITLE} TEXT NOT NULL, " +
                "${DbContract.COLUMN_AUTHOR} TEXT NOT NULL, " +
                "${DbContract.COLUMN_DATE} TEXT NOT NULL, " +
                "${DbContract.COLUMN_DESC} TEXT NOT NULL, " +
                "${DbContract.COLUMN_MEDIA_TYPE} TEXT NOT NULL, " +
                "${DbContract.COLUMN_URL_HD} TEXT NOT NULL);"

        const val APOD_TITLE = "title"
        const val APOD_AUTHOR = "copyright"
        const val APOD_DATE = "date"
        const val APOD_DESC = "explanation"
        const val APOD_URL = "url" // Primary Key
        const val APOD_URL_HD = "hdurl"
        const val APOD_MEDIA_TYPE = "media_type"

        private val TAG = "[${DbHelper::class.java.simpleName}] "
    }

    private val db: SQLiteDatabase = writableDatabase

    override fun onCreate(db: SQLiteDatabase?) {


        db?.execSQL(SQL_CREATE_APOD_TABLE)
        Log.d(APP_TAG, "$TAG onCreate: created DB successfully!")
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    fun storeJsonToDb(jsonArr: JSONArray) {
        storeJsonToDb(db, jsonArr)
    }

    private fun storeJsonToDb(db: SQLiteDatabase?, jsonArr: JSONArray) {
        try {
            var i = 0

            while (i < jsonArr.length()) {

                val jsonObject = jsonArr.getJSONObject(i)

                val title = jsonObject.optString(APOD_TITLE)
                val author = jsonObject.optString(APOD_AUTHOR)
                val date = jsonObject.optString(APOD_DATE)
                val desc = jsonObject.optString(APOD_DESC)
                val url = jsonObject.optString(APOD_URL)
                val urlhd = jsonObject.optString(APOD_URL_HD)
                val mediaType = jsonObject.optString(APOD_MEDIA_TYPE)

                if (mediaType == IMAGE_TYPE && url != "") {
                    val apodValues = ContentValues()

                    with(apodValues) {
                        put(COLUMN_TITLE, title)
                        put(COLUMN_AUTHOR, author)
                        put(COLUMN_DATE, date)
                        put(COLUMN_DESC, desc)
                        put(COLUMN_URL, url)
                        put(COLUMN_URL_HD, urlhd)
                        put(COLUMN_MEDIA_TYPE, mediaType)
                    }

                    db?.insert(TABLE_NAME, null, apodValues)

                    Log.d(APP_TAG, "$TAG storeJsonToDb: inserted successfully $apodValues")
                }

                i++
            }
        } catch (jsonex: JSONException){
            Log.d(APP_TAG, "$TAG storeJsonToDb: caught exception!")
            jsonex.printStackTrace()
        }
    }

    fun deleteApodEntries(){
        try {
            db.delete(TABLE_NAME, null, null)
            Log.d(APP_TAG, "$TAG deleteApodEntries: successfully deleted entries!")
        } catch (sqlex: SQLiteException){
            Log.d(APP_TAG, "$TAG deleteApodEntries: caught exception!")
            sqlex.printStackTrace()
        }
    }

    fun getApodJsonArrayFromDb() : JSONArray {
        val sb = StringBuilder()
        sb.append("[")

        lateinit var cursor: Cursor
        try{
            cursor = this.readableDatabase.rawQuery(QUERY_APODS, null)
            if (cursor.moveToFirst()) {
                val desc = cursor.getString(cursor.getColumnIndex(COLUMN_DESC))

                sb.append(
                    "{\"title\":\"${cursor.getString(cursor.getColumnIndex(COLUMN_TITLE))}\"," +
                            "\"copyright\":\"${cursor.getString(cursor.getColumnIndex(COLUMN_AUTHOR))}\"," +
                            "\"date\":\"${cursor.getString(cursor.getColumnIndex(COLUMN_DATE))}\"," +
                            "\"explanation\":\"${desc.escapeCharacters()}\"," +
                            "\"url\":\"${cursor.getString(cursor.getColumnIndex(COLUMN_URL))}\"," +
                            "\"hdurl\":\"${cursor.getString(cursor.getColumnIndex(COLUMN_URL_HD))}\"," +
                            "\"media_type\":\"${cursor.getString(cursor.getColumnIndex(COLUMN_MEDIA_TYPE))}\"}"
                )
            }

            while(cursor.moveToNext()){
                val desc = cursor.getString(cursor.getColumnIndex(COLUMN_DESC))

                sb.append(",{\"title\":\"${cursor.getString(cursor.getColumnIndex(COLUMN_TITLE))}\"," +
                        "\"copyright\":\"${cursor.getString(cursor.getColumnIndex(COLUMN_AUTHOR))}\"," +
                        "\"date\":\"${cursor.getString(cursor.getColumnIndex(COLUMN_DATE))}\"," +
                        "\"explanation\":\"${desc.escapeCharacters()}\"," +
                        "\"url\":\"${cursor.getString(cursor.getColumnIndex(COLUMN_URL))}\"," +
                        "\"hdurl\":\"${cursor.getString(cursor.getColumnIndex(COLUMN_URL_HD))}\"," +
                        "\"media_type\":\"${cursor.getString(cursor.getColumnIndex(COLUMN_MEDIA_TYPE))}\"}")
            }
        } catch (sqlex: SQLiteException){
            Log.d(APP_TAG, "$TAG getApodJsonArrayFromDb: caught exception!")

        } finally {
            cursor.close()
        }

        sb.append("]")

        Log.d(APP_TAG, "$TAG getApodJsonArrayFromDb: $sb")

        return JSONArray(sb.toString())
    }

    private fun String.escapeCharacters() : String{
        return replace("\n", " ").replace("\'", " \\\'").replace("\"", " \\\"")
    }
}