package com.johnriggsdev.malauzai.api

import com.johnriggsdev.malauzai.util.Constants.Companion.NASA_API_APOD
import com.johnriggsdev.malauzai.util.Constants.Companion.NASA_API_COUNT
import com.johnriggsdev.malauzai.util.Constants.Companion.NASA_API_KEY
import com.johnriggsdev.malauzai.util.Constants.Companion.NASA_BASE_URL
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ApiServiceInterface {
    @GET("$NASA_API_APOD$NASA_API_KEY&$NASA_API_COUNT")
    suspend fun getApods(): Response<ResponseBody>

    companion object {
        fun create(): ApiServiceInterface {

            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(NASA_BASE_URL)
                .build()

            return retrofit.create(ApiServiceInterface::class.java)
        }
    }
}

