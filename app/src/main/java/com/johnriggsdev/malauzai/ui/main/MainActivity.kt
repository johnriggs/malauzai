package com.johnriggsdev.malauzai.ui.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import com.johnriggsdev.malauzai.models.Apod
import com.johnriggsdev.malauzai.R
import com.johnriggsdev.malauzai.di.components.DaggerActivityComponent
import com.johnriggsdev.malauzai.di.modules.ActivityModule
import javax.inject.Inject

import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), MainContract.View{

    @Inject
    lateinit var presenter: MainContract.Presenter

    companion object {
        private const val APOD_ARRAY = "apodArray"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        injectDependency()

        presenter.attach(this)

        savedInstanceState?.let{
            @Suppress("UNCHECKED_CAST")
            if (it.containsKey(APOD_ARRAY)) {
                presenter.onActivityRecreated(
                    it.getParcelableArray(APOD_ARRAY) as Array<Apod>
                )
            } else {
                presenter.onNewActivityCreated()
            }
        } ?: presenter.onNewActivityCreated()
    }

    override fun onResume() {
        super.onResume()

        srl_main.setOnRefreshListener {
            presenter.onSwipeRefreshTriggered()
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.onDestroy()
    }

    private fun injectDependency() {
        val activityComponent = DaggerActivityComponent.builder()
            .activityModule(ActivityModule(this))
            .build()

        activityComponent.inject(this)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        val apodArray = presenter.getApodsForScreenRotation()
        if (apodArray.isNotEmpty()) {
            outState?.putParcelableArray(APOD_ARRAY, apodArray)
        }
    }

    override fun setupRecyclerView(apods: Array<Apod>) {
        val recycler = rv_main
        recycler.layoutManager = LinearLayoutManager(this)
        recycler.adapter = MainAdapter(apods, this)
        recycler.setRecyclerListener(MainAdapter.RecylerViewListenerImpl(this))
    }

    override fun updateRecyclerView(apods: Array<Apod>) {
        (rv_main.adapter as MainAdapter).updateData(apods)
    }

    override fun showProgressBar(visible: Boolean) {
        when(visible){
            true -> progress_main.visibility = View.VISIBLE
            else -> progress_main.visibility = View.GONE
        }
    }

    override fun showErrorToast(message: String) {
        Toast.makeText(this, message, LENGTH_SHORT).show()
    }

    override fun hideSwipeRefreshSpinner() {
        srl_main.isRefreshing = false
    }
}
