package com.johnriggsdev.malauzai.ui.main

import com.johnriggsdev.malauzai.models.Apod
import com.johnriggsdev.malauzai.ui.BaseContract

interface MainContract {
    interface View: BaseContract.View {
        fun setupRecyclerView(apods: Array<Apod>)
        fun updateRecyclerView(apods: Array<Apod>)
        fun showProgressBar(visible: Boolean)
        fun showErrorToast(message: String)
        fun hideSwipeRefreshSpinner()
    }

    interface Presenter: BaseContract.Presenter<MainContract.View> {
        fun onActivityRecreated(apods: Array<Apod>)
        fun onNewActivityCreated()
        fun onSwipeRefreshTriggered()
        fun getApodsForScreenRotation() : Array<Apod>
    }

    interface Repository: BaseContract.Repository {
        fun getApodsFromDb()
        fun getApodsFromApi(refreshing: Boolean)
        fun setNetworkCallback(callback: ApodDataCallback)
    }
}