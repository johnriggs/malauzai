package com.johnriggsdev.malauzai.ui

interface BaseContract {
    interface Presenter<in T> {
        fun attach(view :T)
        fun onDestroy()
    }

    interface View

    interface Repository {
        fun getString(string: Int) : String
    }
}