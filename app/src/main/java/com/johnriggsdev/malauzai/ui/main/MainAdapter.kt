package com.johnriggsdev.malauzai.ui.main

import android.content.Context
import android.content.res.Configuration
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.johnriggsdev.malauzai.R
import com.johnriggsdev.malauzai.models.Apod
import com.johnriggsdev.malauzai.util.Constants.Companion.EMPTY_STRING
import kotlinx.android.synthetic.main.row_main_rv_landscape.view.*

class MainAdapter(private var apods : Array<Apod>, private val context : Context) : RecyclerView.Adapter<MainAdapter.MainViewHolder>() {

    companion object {
        const val MAX_LINES = 4
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val view = when(context.resources.configuration.orientation){
            Configuration.ORIENTATION_LANDSCAPE -> LayoutInflater.from(context).inflate(R.layout.row_main_rv_landscape, parent, false)
            else -> LayoutInflater.from(context).inflate(R.layout.row_main_rv_portrait, parent, false)
        }
        return MainViewHolder(view)
    }

    override fun getItemCount(): Int {
        return apods.size
    }

    override fun onBindViewHolder(holder: MainViewHolder, postition: Int) {
        val apod = apods[postition]

        holder.view.tv_title.text = apod.title

        @Suppress("UsePropertyAccessSyntax")
        apod.author?.let {
            when (it){
                EMPTY_STRING -> holder.view.tv_author_value.text = context.resources.getString(R.string.author_unknown)
                else -> holder.view.tv_author_value.text = it
            }
        } ?: holder.view.tv_author_value.setText(context.resources.getString(R.string.author_unknown))

        holder.view.tv_date_value.text = apod.date

        holder.view.tv_desc_value.maxLines = MAX_LINES
        holder.view.tv_desc_value.text = apod.description
        holder.view.tv_expand.text = context.resources.getString(R.string.expand)

        holder.view.tv_expand.setOnClickListener {
            if (MAX_LINES >= holder.view.tv_desc_value.lineCount){
                holder.view.tv_expand.visibility = View.GONE
            } else {
                if (holder.view.tv_desc_value.maxLines == holder.view.tv_desc_value.lineCount) {
                    holder.view.tv_desc_value.maxLines = MAX_LINES
                    holder.view.tv_expand.text = context.resources.getString(R.string.expand)
                } else {
                    holder.view.tv_desc_value.maxLines = holder.view.tv_desc_value.lineCount
                    holder.view.tv_expand.text = context.resources.getString(R.string.collapse)
                }
            }
        }

        Glide.with(context).load(apod.url).apply(RequestOptions()
            .fitCenter().placeholder(R.drawable.nasa_logo_grey)).into(holder.view.iv_apod)

        holder.view.iv_apod.contentDescription = apod.title
    }

    fun updateData(apods : Array<Apod>){
        this.apods = apods
        notifyDataSetChanged()
    }

    class MainViewHolder(val view : View) : RecyclerView.ViewHolder(view)

    class RecylerViewListenerImpl(private val context: Context) : RecyclerView.RecyclerListener {
        override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
            Glide.with(context).clear((holder as MainAdapter.MainViewHolder).view.iv_apod)
        }
    }
}




