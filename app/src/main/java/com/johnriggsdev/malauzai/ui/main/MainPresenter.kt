package com.johnriggsdev.malauzai.ui.main

import android.util.Log
import com.johnriggsdev.malauzai.R
import com.johnriggsdev.malauzai.models.Apod
import com.johnriggsdev.malauzai.util.Constants.Companion.APP_TAG

class MainPresenter(private val repo: MainContract.Repository): MainContract.Presenter, ApodDataCallback {

    private var view : MainContract.View? = null

    private var apodArray : Array<Apod> = arrayOf()

    override fun attach(view: MainContract.View) {
        this.view = view
        repo.setNetworkCallback(this)
    }

    override fun onDestroy(){
        this.view = null
    }

    override fun onApiError(message: String) {
        view?.showErrorToast(message)
    }

    override fun getApodsForScreenRotation(): Array<Apod> {
        return apodArray
    }

    override fun onActivityRecreated(apods: Array<Apod>) {
        Log.d(APP_TAG, "onActivityRecreated")
        this.apodArray = apods

        view?.setupRecyclerView(apods)
        view?.showProgressBar(false)
    }

    override fun onNewActivityCreated() {
        initializeViews()
    }

    override fun onSwipeRefreshTriggered() {
        view?.hideSwipeRefreshSpinner()
        view?.showProgressBar(true)
        getApodsFromApi(true)
    }

    override fun onApodsApiSuccess(apods : Array<Apod>, refreshing: Boolean) {
        Log.d(APP_TAG, "initializeViews API Success")

        this.apodArray = apods

        if (refreshing){
            view?.updateRecyclerView(apodArray)
        } else {
            view?.setupRecyclerView(apodArray)
        }
        view?.showProgressBar(false)
    }

    override fun onApodsApiFailure() {
        Log.w(APP_TAG, "initializeViews API Failure")
        view?.showErrorToast(repo.getString(R.string.no_api_photos))
        view?.showProgressBar(false)
    }

    override fun onApodsDbSuccess(apods : Array<Apod>) {
        Log.d(APP_TAG, "initializeViews DB Success")

        this.apodArray = apods

        view?.setupRecyclerView(apodArray)
        view?.showProgressBar(false)
    }

    override fun onApodsDbFailure() {
        Log.w(APP_TAG, "initializeViews DB Failure")
        getApodsFromApi(false)
    }

    private fun initializeViews(){
        view?.showProgressBar(true)
        getApodsFromDb()
    }

    private fun getApodsFromApi(refreshing: Boolean){
        repo.getApodsFromApi(refreshing)
    }

    private fun getApodsFromDb(){
        repo.getApodsFromDb()
    }

    fun getViewForTesting() : MainContract.View?{
        return view
    }

    fun getArrayForTesting() : Array<Apod> {
        return apodArray
    }
}

interface ApodDataCallback {
    fun onApodsDbSuccess(apods : Array<Apod>)
    fun onApodsDbFailure()
    fun onApodsApiSuccess(apods : Array<Apod>, refreshing: Boolean)
    fun onApodsApiFailure()
    fun onApiError(message: String)
}