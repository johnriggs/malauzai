package com.johnriggsdev.malauzai.ui.main

import android.util.Log
import com.google.gson.GsonBuilder
import com.johnriggsdev.malauzai.api.ApiServiceInterface
import com.johnriggsdev.malauzai.app.MalauzaiApp
import com.johnriggsdev.malauzai.data.DbHelper
import com.johnriggsdev.malauzai.models.Apod
import com.johnriggsdev.malauzai.util.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONException
import retrofit2.HttpException

class MainRepository : MainContract.Repository{

    private lateinit var apodCallback: ApodDataCallback

    override fun setNetworkCallback(callback: ApodDataCallback) {
        this.apodCallback = callback
    }

    override fun getApodsFromApi(refreshing: Boolean){
        var apods : Array<Apod> = arrayOf()

        CoroutineScope(Dispatchers.IO).launch{
            try {
                val response = ApiServiceInterface.create().getApods()

                if (response.isSuccessful) {
                    val responseString = response.body()?.string() ?: ""

                    val jsonArr = JSONArray(responseString)

                    CoroutineScope(Dispatchers.Default).launch{
                        persistResponseToDb(jsonArr)
                    }

                    apods = getArrayFromString(responseString)

                } else {
                    CoroutineScope(Dispatchers.Main).launch {
                        apodCallback.onApiError("API Error: ${response.code()} ${response.message()}")
                    }
                }
            } catch (e: HttpException) {
                CoroutineScope(Dispatchers.Main).launch {
                    apodCallback.onApiError("API Exception ${e.message}")
                }
            } catch (e: Throwable) {
                CoroutineScope(Dispatchers.Main).launch {
                    apodCallback.onApiError("API Error: ${e.localizedMessage}")
                }
            }

            CoroutineScope(Dispatchers.Main).launch {
                if (apods.isNotEmpty()) {
                    apodCallback.onApodsApiSuccess(apods, refreshing)
                } else {
                    apodCallback.onApodsApiFailure()
                }
            }
        }
    }

    override fun getApodsFromDb(){
        var apodArray: Array<Apod>

        CoroutineScope(Dispatchers.Default).launch {
            val dbHelper = DbHelper(MalauzaiApp.instance)
            val jsonArr = dbHelper.getApodJsonArrayFromDb()
            apodArray = getArrayFromString(jsonArr.toString())

            Log.d(Constants.APP_TAG, "apod: $apodArray")

            CoroutineScope(Dispatchers.Main).launch {
                if (apodArray.isNotEmpty()) {
                    apodCallback.onApodsDbSuccess(apodArray)
                } else {
                    apodCallback.onApodsDbFailure()
                }
            }
        }
    }

    override fun getString(string: Int) : String {
        return MalauzaiApp.instance.getString(string)
    }

    private fun getArrayFromString(string: String): Array<Apod>{
        var apods : Array<Apod> = arrayOf()

        val gson = GsonBuilder().create()
        try {
            apods = gson.fromJson(string, Array<Apod>::class.java)
        } catch (jsonex: JSONException) {
            CoroutineScope(Dispatchers.Main).launch {
                apodCallback.onApiError("API Error: API returned malformed JSON")
            }
        }

        return apods
    }

    private fun persistResponseToDb(jsonArr: JSONArray){
        val dbHelper = DbHelper(MalauzaiApp.instance)
        dbHelper.deleteApodEntries()
        dbHelper.storeJsonToDb(jsonArr)
    }
}